FROM opsperator/apache

# Apache Proxy image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="Apache 2.4 LLNG/LDAP/OIDC Auth Proxy" \
      io.k8s.display-name="Apache 2.4 Auth Proxy" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="apache,apache2,apache24,llng,llng2" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-authproxy" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.4"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && if test "$DO_UPGRADE"; then \
	echo "# Updatng Base Image" \
	&& apt-get -y update \
	&& apt-get -y upgrade \
	&& apt-get -y dist-upgrade \
	&& apt-get clean; \
    fi \
    && echo "# Enabling Proxy Modules" \
    && a2enmod proxy proxy_http proxy_wstunnel proxy_balancer ldap authnz_ldap \
	auth_openidc \
    && echo "# Fixing permissions" \
    && for dir in /etc/lemonldap-ng /etc/ldap; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/etc/ldap/ldap.conf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-proxy.sh"]
USER 1001
