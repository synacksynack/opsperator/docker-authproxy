# k8s AuthProxy

Apache Proxy authenticating users against LLNG or LDAP, forked from
https://github.com/Worteks/docker-authproxy

Diverts from https://gitlab.com/synacksynack/opsperator/docker-apache

Build with:

```
$ make build
```

Depends on LDAP & LLNG images

Test with:

```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description             | Default                                                     | Inherited From    |
| :---------------------------- | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_IGNORE_OPENLDAP`     | Ignore LemonLDAP autoconf  | undef                                                       | opsperator/apache |
|  `APACHE_REMOTEUSER_HEADER`   | Apache Header storing RU   | `X-Forwarded-User`                                          |                   |
|  `AUTH_METHOD`                | Apache Auth Method         | `lemon`, could be `lemon`, `ldap`, `oidc` or `none`         | opsperator/apache |
|  `DEVOPS_HANDLER`             | LemonLDAP-NG rules.json    | undef                                                       |                   |
|  `DO_WEBSOCKETS`              | WebSocket Proxy Toggle     | undef                                                       |                   |
|  `OIDC_CALLBACK_URL`          | OpenID Callback Path       | `/oauth2/callback`                                          |                   |
|  `OIDC_CLIENT_ID`             | OpenID Client ID           | `changeme`                                                  |                   |
|  `OIDC_CLIENT_SECRET`         | OpenID Client Secret       | `secret`                                                    |                   |
|  `OIDC_CRYPTO_SECRET`         | OpenID Crypto Secret       | `secret`                                                    |                   |
|  `OIDC_HTTP_TIMEOUT`          | OpenID HTTP Timeout        | `5`                                                         |                   |
|  `OIDC_META_URL`              | OpenID Meta Path           | `/.well-known/openid-configuration`                         |                   |
|  `OIDC_PORTAL`                | OpenID Portal              | `https://auth.$OPENLDAP_DOMAIN`                             |                   |
|  `OIDC_REMOTE_USER_CLAIM`     | OpenID Remote User Claim   | `sub`                                                       |                   |
|  `OIDC_SKIP_TLS_VERIFY`       | OpenID Skip TLS Verify     | undef                                                       |                   |
|  `OIDC_TOKEN_ENDPOINT_AUTH`   | OpenID Auth Method         | `client_secret_basic`                                       |                   |
|  `ONLY_TRUST_KUBE_CA`         | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`              | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`    | OpenLDAP Bind DN Prefix    | `cn=authproxy,ou=services`                                  | opsperator/apache |
|  `OPENLDAP_BIND_PW`           | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`    | OpenLDAP Conf DN Prefix    | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name       | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`              | OpenLDAP Backend Address   | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`              | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`             | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_SEARCH`            | OpenLDAP Search URI        | `ou=users,$OPENLDAP_BASE?uid?sub?(<objectclass>)`           |                   |
|  `OPENLDAP_USERS_OBJECTCLASS` | OpenLDAP Users ObjectClass | `inetOrgPerson`                                             | opsperator/apache |
|  `PROXY_BACKEND_BASE`         | Proxy Backend Base         | `/`                                                         |                   |
|  `PROXY_BACKEND_HOST`         | Proxy Backend Host         | `127.0.0.1`                                                 |                   |
|  `PROXY_BACKEND_PORT`         | Proxy Backend Port         | `8081`                                                      |                   |
|  `PROXY_BACKEND_PROTO`        | Proxy Backend Proto        | `http`                                                      |                   |
|  `PROXY_HTTP_PORT`            | Proxy Frontend Port        | `8080`                                                      |                   |
|  `PROXY_SERVER_NAME`          | Proxy ServerName           | server hostname                                             |                   |
|  `PING_PATH`                  | Proxy healthcheck url      | undef - if set, `/PING_PATH` bypasses auth                  |                   |
|  `PING_ROOT`                  | Proxy healthcheck docroot  | `/var/www/html` - alternate DocumentRoot for health checks  |                   |
|  `PUBLIC_PROTO`               | Proxy Frontend Proto       | `http`                                                      | opsperator/apache |
|  `SERVE_DIRECTORY`            | Local Directory to serve   | undef, setting one disables proxying                        |                   |
|  `WS_PATH`                    | WebSocket Path to Proxy    | undef, ProxyPass `/WS_PATH` instead of Rewrite Upgraded     |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                    | Inherited From    |
| :------------------ | ------------------------------ | ----------------- |
|  `/certs`           | Apache Certificate (optional)  | opsperator/apache |
