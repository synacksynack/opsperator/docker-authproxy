#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

APACHE_REMOTEUSER_HEADER=${APACHE_REMOTEUSER_HEADER:-X-Forwarded-User}
AUTH_METHOD=${AUTH_METHOD:-lemon}
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_HTTP_TIMEOUT=${OIDC_HTTP_TIMEOUT:-5}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_REMOTE_USER_CLAIM=${OIDC_REMOTE_USER_CLAIM:-sub}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=authproxy,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$OPENLDAP_SEARCH" -a "$AUTH_METHOD" = ldap; then
    OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
    OPENLDAP_SEARCH="ou=users,$OPENLDAP_BASE?uid?sub?(&(objectClass=$OPENLDAP_USERS_OBJECTCLASS)(!(pwdAccountLockedTime=*)))"
fi
PING_PATH=${PING_PATH:-}
PING_ROOT=${PING_ROOT:-/var/www/html}
PROXY_BACKEND_BASE=${PROXY_BACKEND_BASE:-/}
PROXY_BACKEND_HOST=${PROXY_BACKEND_HOST:-127.0.0.1}
PROXY_BACKEND_PORT=${PROXY_BACKEND_PORT:-8081}
PROXY_BACKEND_PROTO=${PROXY_BACKEND_PROTO:-http}
PROXY_HTTP_PORT=${PROXY_HTTP_PORT:-8080}
test -z "$PROXY_SERVER_NAME" && PROXY_SERVER_NAME=`hostname 2>/dev/null || localhost`
export APACHE_DOMAIN=$PROXY_SERVER_NAME
export APACHE_HTTP_PORT=$PROXY_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
if test -z "$PUBLIC_PROTO"; then
    if test "$SSL_INCLUDE" = no-ssl; then
	PUBLIC_PROTO=http
    else
	PUBLIC_PROTO=https
    fi
fi
export PUBLIC_PROTO
export RESET_TLS=false
if test "$AUTH_METHOD" = oidc -a -z "$OIDC_PORTAL"; then
    OIDC_PORTAL=$PUBLIC_PROTO://auth.$OPENLDAP_DOMAIN
fi

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo Generates VirtualHost Configuration
    (
	if test "$AUTH_METHOD" = lemon; then
	    cat <<EOF
PerlOptions +GlobalRequest
PerlModule Lemonldap::NG::Handler::ApacheMP2
EOF
	fi
	cat <<EOF
<VirtualHost *:$PROXY_HTTP_PORT>
    ServerName $PROXY_SERVER_NAME
    CustomLog /dev/stdout modremoteip
    Include "$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
EOF
	if test "$AUTH_METHOD" = lemon; then
	    cat <<EOF
    PerlHeaderParserHandler Lemonldap::NG::Handler::ApacheMP2
EOF
	elif test "$AUTH_METHOD" = oidc; then
	    cat <<EOF
    OIDCClientID $OIDC_CLIENT_ID
    OIDCClientSecret $OIDC_CLIENT_SECRET
    OIDCCryptoPassphrase $OIDC_CRYPTO_SECRET
    OIDCProviderMetadataURL $OIDC_PORTAL$OIDC_META_URL
    OIDCProviderTokenEndpointAuth $OIDC_TOKEN_ENDPOINT_AUTH
    OIDCRedirectURI $PUBLIC_PROTO://$PROXY_SERVER_NAME$OIDC_CALLBACK_URL
    OIDCRemoteUserClaim $OIDC_REMOTE_USER_CLAIM
EOF
	fi
	if test -s /rewrite-rules -o "$AUTH_METHOD" = openid -o "$DO_WEBSOCKETS"; then
	    cat <<EOF
    RewriteEngine On
EOF
	    test -s /rewrite-rules && cat /rewrite-rules
	fi
	if test "$PROXY_BACKEND_PROTO" = https; then
	    cat <<EOF
    SSLProxyEngine On
EOF
	fi
	if test "$AUTH_METHOD" = lemon; then
	    cat <<EOF
    SetEnvIfNoCase $APACHE_REMOTEUSER_HEADER "(.*)" REMOTE_USER=\$1
EOF
	    if test "$DEVOPS_HANDLER"; then
		cat <<EOF
    <Location "/rules.json">
	SetHandler None
	Require all granted
    </Location>
    Alias /rules.json /var/www/rules.json
EOF
	    fi
	elif test "$AUTH_METHOD" = openid -o "$AUTH_METHOD" = ldap; then
	    cat <<EOF
    RequestHeader set $APACHE_REMOTEUSER_HEADER expr=%{REMOTE_USER}
EOF
	fi
	if test "$DO_WEBSOCKETS"; then
	    WS_PROTO=ws
	    if test "$PROXY_BACKEND_PROTO" = https; then
		WS_PROTO=wss
	    fi
	    if test "$WS_PATH"; then
		if echo "$WS_PATH" | grep ^/ >/dev/null; then
		    WS_PATH=`echo "$WS_PATH" | sed 's|^/||'`
		fi
		cat <<EOF
    ProxyPass $PROXY_BACKEND_BASE$WS_PATH $WS_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE$WS_PATH keepalive=On timeout=600 retry=1 acquire=3000
    ProxyPassReverse $PROXY_BACKEND_BASE$WS_PATH $WS_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE$WS_PATH
    ProxyPassReverseCookieDomain $PROXY_BACKEND_HOST %{HTTP:Host}
    ProxyPass / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
    ProxyPassReverse / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
EOF
	    else
		cat <<EOF
    RewriteCond %{HTTP:Upgrade} websocket [NC]
    RewriteRule /(.*) "$WS_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE\$1" [P,QSA,L]
    RewriteCond %{HTTP:Upgrade} =websocket [NC]
    RewriteRule /(.*) "$WS_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE\$1" [P,QSA,L]
    RewriteCond %{REQUEST_URI} ^/socket.io [NC]
    RewriteCond %{QUERY_STRING} transport=websocket [NC]
    RewriteRule /(.*) "$WS_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE\$1" [P,QSA,L]
    ProxyPass / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
    ProxyPassReverse / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
EOF
	    fi
	elif test "$SERVE_DIRECTORY"; then
	    if ! test -d "$SERVE_DIRECTORY"; then
		echo "CRITICAL: $SERVE_DIRECTORY is not a directory"
	    fi
	    if test "$DIRECTORY_INDEX"; then
		cat <<EOF
    DirectoryIndex $DIRECTORY_INDEX
EOF
	    fi
	    cat <<EOF
    DocumentRoot $SERVE_DIRECTORY
EOF
	else
	    cat<<EOF
    ProxyPass / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
    ProxyPassReverse / $PROXY_BACKEND_PROTO://$PROXY_BACKEND_HOST:$PROXY_BACKEND_PORT$PROXY_BACKEND_BASE
EOF
	fi
	if test "$SERVE_DIRECTORY" -a "$DIRECTORY_INDEX"; then
	    cat <<EOF
    <Location />
	AllowOverride all
	Options +Indexes
EOF
	elif test "$SERVE_DIRECTORY"; then
	    cat <<EOF
    <Location />
	AllowOverride all
EOF
	else
	    cat <<EOF
    <Location />
EOF
	    fi
	if test "$AUTH_METHOD" = ldap; then
	    cat <<EOF
	AuthType Basic
	AuthBasicProvider ldap
	AuthLDAPBindDN "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE"
	AuthLDAPBindPassword "$OPENLDAP_BIND_PW"
	AuthLDAPURL "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/$OPENLDAP_SEARCH" NONE
	AuthName "LDAP Auth"
	Require valid-user
    </Location>
EOF
	elif test "$AUTH_METHOD" = oidc; then
	    if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
		OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	    fi
	    CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	    CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	    if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
		CALLBACK_ROOT_SUB_COUNT=2
	    fi
	    CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
	    cat <<EOF
	AuthType openid-connect
	Require valid-user
    </Location>
EOF
	    if test "$SERVE_DIRECTORY"; then
		cat <<EOF
    <Location /$CALLBACK_ROOT_URL/>
	AuthType openid-connect
	Require valid-user
    </Location>
EOF
	    else
		cat <<EOF
    <Location /$CALLBACK_ROOT_URL/>
	AuthType openid-connect
	ProxyPass !
	Require valid-user
    </Location>
EOF
	    fi
	    if test "$PING_PATH"; then
		if echo "$PING_PATH" | grep ^/ >/dev/null; then
		    PING_PATH=`echo $PING_PATH | sed 's|^/||'`
		fi
		if ! test -d "$PING_ROOT"; then
		    PING_ROOT=/var/www/html
		fi
		if test "$SERVE_DIRECTORY"; then
		    cat <<EOF
    Alias /$PING_PATH $PING_ROOT
    <Location /$PING_PATH/>
	Require all granted
    </Location>
EOF
		else
		    cat <<EOF
    Alias /$PING_PATH $PING_ROOT
    <Location /$PING_PATH/>
	ProxyPass !
	Require all granted
    </Location>
EOF
		fi
	    fi
	else
	    cat <<EOF
	Require all granted
    </Location>
EOF
	fi
	echo "</VirtualHost>"
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
fi

if test "$AUTH_METHOD" = ldap; then
    export APACHE_IGNORE_OPENLDAP=yay
    if ! test -s /etc/ldap/ldap.conf; then
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    echo "LDAPVerifyServerCert Off" \
		>>/etc/apache2/sites-enabled/003-vhosts.conf
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	never
EOF
	elif test "$OPENLDAP_PROTO" = ldaps; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	demand
EOF
	fi >/etc/ldap/ldap.conf
    fi
elif test "$AUTH_METHOD" = oidc; then
    if test "$OIDC_SKIP_TLS_VERIFY"; then
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    else
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    fi
    sed -i \
	-e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	-e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	-e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	-e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	-e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	-e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	-e "s|^#*[ ]*OIDCHTTPTimeoutShort.*|OIDCHTTPTimeoutShort $OIDC_HTTP_TIMEOUT|" \
	/etc/apache2/mods-enabled/auth_openidc.conf
    export APACHE_IGNORE_OPENLDAP=yay
elif test "$AUTH_METHOD" = lemon; then
    if test "$DEVOPS_HANDLER"; then
	if test -s /lemon-rules.json; then
	    ln -sf /lemon-rules.json /var/www/rules.json
	elif ! test -s /var/www/rules.json; then
	    cat <<EOF >/var/www/rules.json
{
    "rules": {
	    "^/admin": "\$uid eq 'admin'",
	    "default": "accept'
	},
    "headers": {
	    "Auth-User": "\$uid"
	}
    }
EOF
	fi
    fi
elif test "$AUTH_METHOD" = none; then
    export APACHE_IGNORE_OPENLDAP=yay
fi

unset PROXY_HTTP_PORT PROXY_SERVER_NAME PROXY_BACKEND_PROTO OIDC_PORTAL \
    PROXY_BACKEND_BASE PROXY_BACKEND_HOST PROXY_BACKEND_PORT PING_ROOT \
    PROXY_BACKEND_BASE REQUIRE OPENLDAP_SEARCH OIDC_CALLBACK_URL PING_PATH \
    OIDC_CLIENT_ID OIDC_CLIENT_SECRET OIDC_CRYPTO_SECRET OIDC_META_URL WS_PATH \
    WS_PROTO DO_WEBSOCKETS OIDC_TOKEN_ENDPOINT_AUTH CALLBACK_ROOT_SUB_COUNT \
    CALLBACK_ROOT_URL OIDC_SKIP_TLS_VERIFY

. /run-apache.sh
