SKIP_SQUASH?=1
IMAGE=opsperator/authproxy
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: run
run:
	@@docker run -e DEBUG=yayy \
	    -e PROXY_HTTP_PORT=8081 \
	    -e PROXY_SERVER_NAME=whitepages.demo.local \
	    -e PROXY_BACKEND_HOST=localhost \
	    -e PROXY_BACKEND_PORT=8080 \
	    -p 8081:8081 -d $(IMAGE)

.PHONY: oidcdemo
oidcdemo:
	docker run -p 8080:8080 -e DEBUG=yay \
	    -e AUTH_METHOD=oidc \
	    -e OIDC_CALLBACK_URL=/oauth2/callback \
	    -e OIDC_CLIENT_ID=private \
	    -e OIDC_CLIENT_SECRET=tardis \
	    -e OIDC_CRYPTO_SECRET=testcryptosecret \
	    -e OIDC_META_URL=/.well-known/openid-configuration \
	    -e OIDC_PORTAL=https://oidctest.wsweet.org \
	    -e OIDC_TOKEN_ENDPOINT_AUTH=client_secret_basic \
	    -e PING_PATH=/ping \
	    -e PROXY_SERVER_NAME=myapp.example.com \
	    -e PROXY_HTTP_PORT=8080 \
	    -e PUBLIC_PROTO=https \
	    -e SERVE_DIRECTORY=/var/www/html \
	    -d $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "PROXY_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "PROXY_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=opsperator | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=opsperator | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml | oc delete -f- || true
